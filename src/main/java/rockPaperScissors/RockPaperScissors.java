package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true){
            // Imput Choise 
            System.out.println("Let's play round "+ roundCounter);
            roundCounter++;
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String MyChoice = sc.nextLine();
            if (!rpsChoices.contains(MyChoice)){
                System.out.println("I do not understand"+MyChoice + ". Could you try again?");
            }
            
            
            //Computer Choise
            String ComputerChoise = rpsChoices.get(new Random().nextInt(rpsChoices.size()));  

            
            //Compare Choise and Add score
            if (MyChoice.equals(ComputerChoise)) {
                System.out.println("Human chose "+MyChoice+", computer chose "+ComputerChoise+". It's a tie!");
            }else if ((MyChoice.equals("Rock") && ComputerChoise.equals("Scissors")) || 
            (MyChoice.equals("Paper") && ComputerChoise.equals("Rock")) || 
            (MyChoice.equals("Scissors") && ComputerChoise.equals("Paper"))){
                    System.out.println("Human chose "+MyChoice+", computer chose "+ComputerChoise+". Human wins!");
                    humanScore++;
            }else {
                System.out.println("Human chose "+MyChoice+", computer chose "+ComputerChoise+". Computer wins!");
                computerScore++;
                }
            System.out.println("Score: human "+ humanScore+", computer "+ computerScore);
            System.out.println("Do you wish to continue playing? (y/n)?");
            String play = sc.nextLine();
            if (play.equals("n")){
                break;
                
            }
                
        }
        //Continiuse play 
        System.out.println("Bye bye :)");
            }
           
           
        
            
        
            
            
        

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
